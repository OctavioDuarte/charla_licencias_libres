(TeX-add-style-hook
 "2_licenciaYPint"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("standalone" "preview" "border=2bp")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("babel" "spanish") ("inputenc" "utf8") ("geometry" "a1paper")))
   (TeX-run-style-hooks
    "latex2e"
    "standalone"
    "standalone10"
    "babel"
    "inputenc"
    "tikz"
    "xcolor"
    "geometry")
   (TeX-add-symbols
    "comentario"))
 :latex)

