(TeX-add-style-hook
 "s1"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("standalone" "preview" "border=6bp")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("babel" "spanish") ("inputenc" "utf8") ("geometry" "a2paper")))
   (TeX-run-style-hooks
    "latex2e"
    "standalone"
    "standalone10"
    "babel"
    "inputenc"
    "tikz"
    "xcolor"
    "geometry"
    "hyperref")
   (TeX-add-symbols
    "comentario"))
 :latex)

