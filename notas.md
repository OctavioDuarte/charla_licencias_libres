# Licencias Abiertas

## Breve motivación histórica

### Tres Etapas

* Código como conocimiento. Ej: teorema matemático, un tipo de motor.
* Código como mercancía. Ej: Windows, office. Un coche.
* Código como servicio (SaaS). Ej: Android e iOS,  Photoshop.

Concepto de que en el código cerrado se busca que en cualquier situación donde un usuario llegue a emplear el código, sean cuales sean las circunstancias, este acto implique pagar. Espionaje a trabés de las ISP.

Originalmente, los proveedores de código se sentían en desventaja de los proveedores de objetos físicos, ya que su mercancía era especialmente vulnerable al tráfico impago. Con la llegada del SaaS, esto pasó a ser al revés, a tal punto que se intenta imponer esta lógica en el objeto físco, que es inherentemente resinstente a ella (el automóvil digitalizado).

### Relevancia en el caso dispositivos y otros

* Patentes abusivas: artículos científicos, tecnología médica. Datos médicos.
* Invasión de la propiedad privada de los usuarios para controlar patentes.
* Indefensión: caso datos en celulares.
* Caducidad programada, servicios, matenimientos, calibraciones. Tractores, coches.

https://www.gnu.org/licenses/gpl-faq.html#GPLOtherThanSoftware

## Licencias Libres

### Licencia vs Atribución vs Derecho 

* Mostrar sitio de GNU.
* La licencia cubre el uso, no la posesión. Le da y quita derechos al usuario, no restrinje al poseedor del derecho.

### Las 5 libertades

### Licencias CC copyleft

### ¿Se puede vender?

## Licencias Abiertas

### ¿Son "más libres" que las licencias libres? 

### La estrategia "lesser"

### El argumento liberal

### Por qué el argumento del estímulo no es cierto

## Edad de oro del "código abierto"

## Algunas ventajas del código abierto

## Casos donde es importante la libertad de una patente



cita de Marx

https://pijamasurf.com/2018/11/marx_predijo_en_este_parrafo_de_1848_las_redes_sociales_y_el_capitalismo_digital_de_nuestra_era/



# Fuentes

* cita BSD, BSD vs GPL: https://docs.freebsd.org/en/articles/bsdl-gpl/
* Sobre GPL: https://www.gnu.org/licenses/licenses.html
* Licencias de la OSI: https://opensource.org/licenses/
* Sobre las ideas de los coches eléctricos: movimiento right to repair.
https://idle.slashdot.org/story/22/01/08/032246/worst-of-ces-awards-announced-by-right-to-repairprivacy-advocates
* Sobre privacidad, contaminación y falta de control.
https://yro.slashdot.org/story/23/01/07/0329201/cess-worst-in-show-criticized-over-privacy-security-and-environmental-threats?sdsrc=rel
* Frases de Ballmer: https://www.zdnet.com/article/ballmer-i-may-have-called-linux-a-cancer-but-now-i-love-it/
